package com.tsystems.javaschool.tasks.duplicates;


import java.io.*;
import java.util.*;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException("Arguments can not be null");
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
             BufferedWriter writer = new BufferedWriter(new FileWriter(targetFile, true))) {

            Map<String, Integer> map = new HashMap<>();
            String line;
            while (reader.ready()) {
                line = reader.readLine();
                if (map.containsKey(line)) {
                    map.put(line, map.get(line) + 1);
                } else {
                    map.put(line, 1);
                }
            }

            List<String> keys = new ArrayList<>(map.keySet());
            Collections.sort(keys);

            for (String x : keys) {
                writer.write(x + "[" + map.get(x) + "]\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


}
